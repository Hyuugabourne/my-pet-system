package br.com.mps.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.mps.entity.Animal;
import br.com.mps.entity.Raca;
import br.com.mps.service.MyPetService;

@Named
@ViewScoped
public class AnimaisBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EJB
    private MyPetService service;

    private List<Animal> animais = new ArrayList<>();

    private List<Raca> racas = new ArrayList<>();

    private Raca raca = new Raca();

    private Integer index;

    @PostConstruct
    public void init() {
        animais = service.buscaAnimais();
        Animal a = new Animal();
        Raca r = new Raca();
        r.setNome("Raca");
        a.setRaca(r);
        a.setNome("animal");
        animais.add(a);
        racas = service.buscaRacas();
    }

    public String open() {
        return "animais.xhtml";
    }


    public void addAnimal() {
        Animal animal = new Animal();
        animal.setRaca(new Raca());
        animal.setEditando(true);
        animais.add(animal);
    }

    public void salvarRaca() {
        service.salva(raca);
    }

    public void removerAnimal(Animal animal) {
        service.excluirAnimal(animal);
    }

    public void editarAnimal(Animal animal) {
        animal.setEditando(true);
    }

    public void salvarAnimal(Animal animal) {
        animal.setEditando(false);
        service.cadastraAnimal(animal);
    }

    public List<Animal> getAnimais() {
        return animais;
    }

    public void setAnimais(List<Animal> animais) {
        this.animais = animais;
    }

    public List<Raca> getRacas() {
        return racas;
    }

    public void setRacas(List<Raca> racas) {
        this.racas = racas;
    }


    public Raca getRaca() {
        return raca;
    }

    public void setRaca(Raca raca) {
        this.raca = raca;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
