package br.com.mps.controllers;

import br.com.mps.entity.Adocoens;
import br.com.mps.entity.Animal;
import br.com.mps.entity.Interessado;
import br.com.mps.service.MyPetService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@RequestScoped
public class adocoesBean implements Serializable {

    private Adocoens adocao;

    private List<Interessado> interessados;

    @EJB
    private MyPetService service;

    @PostConstruct
    public void init() {
        interessados = new ArrayList<>();
        Interessado interessado = new Interessado();
        interessado.setNome("Matheus");
        interessados.add(interessado);
        interessado = new Interessado();
        interessado.setNome("Joao");
        interessados.add(interessado);
        interessado = new Interessado();
        interessado.setNome("Marcos");
        interessados.add(interessado);
        Animal animal = new Animal();
        animal.setNome("Animalsinho");
        adocao = new Adocoens();
        adocao.setAnimal(animal);
        //interessados = service.buscaInteressados();
    }

    public String open() {
        return "adocoes.xhtml";
    }

    public void cadastrarAdocao() {
        service.cadastraAdocao(adocao);
    }

    public Adocoens getAdocao() {
        return adocao;
    }

    public void setAdocao(Adocoens adocao) {
        this.adocao = adocao;
    }

    public List<Interessado> getInteressados() {
        return interessados;
    }

    public void setInteressados(List<Interessado> interessado) {
        this.interessados = interessado;
    }
}
