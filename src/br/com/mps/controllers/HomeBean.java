package br.com.mps.controllers;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;


@Named
@ViewScoped
public class HomeBean implements Serializable {

	private String nav_active = "active";

	public String open(){
		return "index.xhtml";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getNav_index() {
		return nav_active;
	}

	public void setNav_index(String nav_index) {
		this.nav_active = nav_index;
	}
}
