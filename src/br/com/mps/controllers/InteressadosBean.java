package br.com.mps.controllers;

import br.com.mps.entity.Endereco;
import br.com.mps.entity.Interessado;
import br.com.mps.entity.Telefone;
import br.com.mps.service.MyPetService;
import com.sun.javafx.scene.traversal.ContainerTabOrder;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class InteressadosBean implements Serializable {

    private List<Interessado> interessados;

    private Interessado interessado;

    @EJB
    private MyPetService service;

    @PostConstruct
    public void init(){
        service.buscaInteressados();
    }

    public String open() {
        return "interessados.xhtml";
    }

    public String cadastraInteressado(){
        service.cadastraInteressado(interessado);
        return open();
    }

    public String addInteressado() {
        interessado = new Interessado();
        interessado.setEndereco(new Endereco());
        interessado.setContatos(new ArrayList<>());
        return "editarInteressado.xhtml";
    }

    public String editaInteressado(Interessado inter){
        interessado = inter;
        return "editaInteressado.xhtml";
    }

    public void removerContato(Integer index){
        List<Telefone> contatos = interessado.getContatos();
        Telefone telefone = contatos.get(index);
        boolean remove = contatos.remove(telefone);
        if (remove)
            interessado.setContatos(contatos);

    }

    public void editarContato(Integer index){
        interessado.getContatos().get(index).setEditando(true);
    }

    public void salvarContato(Integer index){
        interessado.getContatos().get(index).setEditando(false);
    }

    public void addContato(){
        if (interessado.getContatos() == null)
            interessado.setContatos(new ArrayList<>());
        Telefone tel = new Telefone();
        tel.setEditando(true);
        interessado.getContatos().add(tel);
    }

    public List<Interessado> getInteressados() {
        return interessados;
    }

    public void setInteressados(List<Interessado> interessados) {
        this.interessados = interessados;
    }

    public Interessado getInteressado() {
        if (interessado == null)
            interessado = new Interessado();
        if (interessado.getEndereco() == null)
            interessado.setEndereco(new Endereco());
        if (interessado.getContatos() == null)
            interessado.setContatos(new ArrayList<>());
        return interessado;
    }

    public void setInteressado(Interessado interessado) {
        this.interessado = interessado;
    }
}
