package br.com.mps.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.mps.entity.*;
import br.com.mps.repository.MyPetRepository;

@Stateless
public class MyPetService {

	@EJB
	private MyPetRepository repository;
	
	public void cadastraAnimal(Animal animal) {
		repository.save(animal);
	}

	public void excluirAnimal(Animal animal) {
		repository.excluir(animal);		
	}

	public void atualizar(Animal animal) {
		repository.atualiza(animal);
	}

	public List<Animal> buscaAnimais() {
		return repository.buscaAnimais();
	}

	public List<Raca> buscaRacas() {
		return repository.buscaRacas();
	}

	public void salva(Raca raca) {
		repository.save(raca);
		
	}

    public List<Interessado> buscaInteressados() {
		return repository.buscaInteressados();
    }

	public void removeContato(Telefone contato) {
		repository.remove(contato);
	}

	public void salvaContato(Telefone contato) {
		repository.atualiza(contato);
	}

	public void cadastraInteressado(Interessado interessado) {
		repository.save(interessado);
	}

	public void cadastraAdocao(Adocoens adocao) {
		repository.save(adocao);
	}
}
