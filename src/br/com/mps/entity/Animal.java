package br.com.mps.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;

@Entity
public class Animal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8590815883455834620L;

	@Id
	@GeneratedValue
	private Integer id;

	private String nome;

	private Date dataEncontro;

	private Boolean estaAdotado;

	@OneToOne(cascade = CascadeType.ALL)
	private Raca raca;

	@Transient
	private transient boolean editando;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataEncontro() {
		return dataEncontro;
	}

	public void setDataEncontro(Date dataEncontro) {
		this.dataEncontro = dataEncontro;
	}

	public Boolean getEstaAdotado() {
		return estaAdotado;
	}

	public void setEstaAdotado(Boolean estaAdotado) {
		this.estaAdotado = estaAdotado;
	}

	public Raca getRaca() {
		return raca;
	}

	public void setRaca(Raca raca) {
		this.raca = raca;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}

}
