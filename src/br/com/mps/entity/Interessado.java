package br.com.mps.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
public class Interessado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	
	private String nome;
	
	private Long cpf;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<Telefone> contatos;

	@OneToOne(cascade = CascadeType.ALL)
	private Endereco endereco;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getCpf() {
		return cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public List<Telefone> getContatos() {
		return contatos;
	}

	public void setContatos(List<Telefone> contatos) {
		this.contatos = contatos;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
}
