package br.com.mps.repository;

import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;


@Stateful
@LocalBean
public class Manager {

	@PersistenceContext(unitName = "defautPU")
    private EntityManager entityManager;	

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void salva(Object obj) {
		entityManager.persist(obj);
		entityManager.flush();
	}
	
	@TransactionAttribute
	public void atualiza(Object obj) {
	    entityManager.merge(obj);
	}
	
	public Object busca(Object obj) {
		obj = entityManager.find(obj.getClass(), obj);
		return obj;
	}
	
	@TransactionAttribute
	public void excluir(Object obj) {
		entityManager.remove(obj);	
	}
	
	@SuppressWarnings("unchecked")
	public List<? extends Object> lista(String query, Map<String, String> parametros) {
		Query nativeQuery = entityManager.createQuery(query);
		if (parametros != null) {
			parametros.forEach((key, param)->{
				nativeQuery.setParameter(key, param);		
			});
		}
		return nativeQuery.getResultList();
	}
}
