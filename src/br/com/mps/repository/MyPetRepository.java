package br.com.mps.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.com.mps.entity.*;

@Stateless
public class MyPetRepository {

	@EJB
	private Manager manager;

	public void save(Adocoens adocao) {
		manager.salva(adocao);		
	}

	public void save(Animal animal) {
		manager.salva(animal);			
	}

	public void save(Interessado interessado) {
		manager.salva(interessado);		
	}

	public void excluir(Animal animal) {
		manager.excluir(animal);		
	}

	public void atualiza(Animal animal) {
		manager.atualiza(animal);		
	}

	@SuppressWarnings("unchecked")
	public List<Animal> buscaAnimais() {
		String query = "SELECT a FROM Animal as a";
		return (List<Animal>) manager.lista(query, null);		
	}

	public List<Raca> buscaRacas() {
		String query = "SELECT a FROM Raca as a";
		return (List<Raca>) manager.lista(query, null);
	}

	public void save(Raca raca) {
		manager.salva(raca);			
	}

    public List<Interessado> buscaInteressados() {
		String query = "SELECT i FROM Interessado as i";
		return (List<Interessado>) manager.lista(query, null);
    }

	public void remove(Telefone contato) {
		manager.excluir(contato);
	}

	public void atualiza(Telefone contato) {
		manager.atualiza(contato);
	}
}
